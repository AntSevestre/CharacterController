using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : InteractionMaster
{
    private Animator animator;

    private void Awake()
    {
        animator = gameObject.transform.GetChild(1).GetComponent<Animator>();
    }

    public override void Interact()
    {
        animator.Play("Open");
    }
}
