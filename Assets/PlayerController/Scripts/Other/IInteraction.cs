public interface IInteraction
{
    bool CanInteract
    {
        get;
    }

    bool AlreadyUse
    {
        get;
    }

    void Interact();
}
