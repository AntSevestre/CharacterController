using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceUIRotation : MonoBehaviour
{
    public Transform target;

    private void Update()
    {
        this.transform.LookAt(target);

        if(gameObject.name == "Target(Clone)")
        {
            target = Camera.main.transform;
        }
    }
}
