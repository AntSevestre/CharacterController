using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionMaster : MonoBehaviour, IInteraction
{
    public bool canInteract;
    public bool alreadyUse;

    public bool CanInteract => canInteract;

    public bool AlreadyUse => alreadyUse;

    public virtual void Interact()
    {
        Debug.Log(gameObject.name);
    }
}
