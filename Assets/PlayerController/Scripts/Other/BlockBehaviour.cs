using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBehaviour : MonoBehaviour
{
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.CompareTag("Wall"))
        {
            hit.gameObject.GetComponent<Explosion>().explode();
            //Destroy(hit.gameObject);
        }
    }
}
