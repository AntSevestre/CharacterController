using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    [HideInInspector] public Animator animator;

    private string currentState;

    private void Awake()
    {
        animator = transform.GetChild(0).GetComponent<Animator>();
    }

    public void SwitchAnimationState(string newState)
    {
        if (currentState == newState)
        {
            return;
        }

        animator.Play(newState);

        currentState = newState;
    }
}

public static class ConstanteVariable
{
    public const string idle = "Idle";
    public const string walking = "Walking";
    public const string jump = "Jump";
    public const string jumpup = "JumpUp";
    public const string charging = "Charging";
    public const string dash = "Dash";
}
