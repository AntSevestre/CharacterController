using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerInteraction : MonoBehaviour
{
    private PlayerInput playerInput;

    public Transform interactionOrigin;
    public LayerMask interactionLayer;
    public float interactionRadius;

    public Canvas displayInterface;
    public bool display;

    public Collider[] actualInteractComponent;
    public Collider[] insideInteractZone;
    public Collider[] outsideInteractZone;

    public LayerMask ennemisLayer;
    public Collider[] ennemis;
    public Collider[] ennemiInside;
    public Collider[] ennemiOutside;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
    }

    private void Update()
    {
        insideInteractZone = Physics.OverlapSphere(interactionOrigin.position, interactionRadius, interactionLayer);

        RefreshDisplay(insideInteractZone, true);

        outsideInteractZone = actualInteractComponent.Except(insideInteractZone).ToArray();

        RefreshDisplay(outsideInteractZone, false);

        ennemiInside = Physics.OverlapSphere(interactionOrigin.position, 15, ennemisLayer);

        RefreshEnnemiList(ennemiInside, true);

        ennemiOutside = ennemis.Except(ennemiInside).ToArray();
    }

    private void RefreshDisplay(Collider[] interactComponent, bool enable)
    {
        foreach (var interact in interactComponent)
        {
            InteractionMaster InteractScript = interact.GetComponent<InteractionMaster>();
            
            if(InteractScript != null)
            {
                displayInterface.enabled = enable;

                if (playerInput.interactFlag)
                {
                    InteractScript.Interact();
                    playerInput.interactFlag = false;
                }
            }
        }
    }

    private void RefreshEnnemiList(Collider[] ennemiTarget, bool enable)
    {
        foreach (var ennemi in ennemiTarget)
        {

        }
    }
}
