using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private Controls controls;

    public float horizontalInput;
    public float verticalInput;

    [Header("input verification")]
    public bool isGrounded;
    public bool jumpFlag;
    public bool dashFlag;
    public bool interactFlag;
    public bool attackLeftFlag;
    public bool attackRightFlag;
    public bool targetFlag;
    public bool SwitchLeftFlag;
    public bool SwitchRightFlag;

    private void Awake()
    {
        if(controls == null)
        {
            controls = new Controls();
        }
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        controls.Locomotion.Jump.performed += ctx => jumpFlag = true;

        controls.Abilitie.Dash.started += ctx => dashFlag = true;
        controls.Abilitie.Dash.canceled += ctx => dashFlag = false;

        controls.Abilitie.Interact.performed += ctx => interactFlag = true;

        controls.Abilitie.AttackLeft.performed += ctx => attackLeftFlag = true;
        controls.Abilitie.AttackRight.performed += ctx => attackRightFlag = true;

        controls.Gameplay.Target.performed += ctx => targetFlag = true;

        if (targetFlag)
        {
            controls.Gameplay.Target.performed += ctx => targetFlag = false;
        }

        controls.Gameplay.SwitchLeft.performed += ctx => SwitchLeftFlag = true;
        controls.Gameplay.SwitchLeft.canceled += ctx => SwitchLeftFlag = false;

        controls.Gameplay.SwitchRight.performed += ctx => SwitchRightFlag = true;
        controls.Gameplay.SwitchRight.canceled += ctx => SwitchRightFlag = false;
    }
}
