using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerAbilites : MonoBehaviour
{
    private PlayerAnimations playerAnimations;
    private PlayerLocomotion playerLocomotion;
    private PlayerInput playerInput;
    private PlayerInteraction playerInteraction;
    public CinemachineFreeLook Vcamera;

    public float maxTimerDash;
    public float maxRangeDash;

    public int index;
    public Collider[] ennemis;
    public GameObject targetVisual;

    private void Awake()
    {
        playerAnimations = GetComponent<PlayerAnimations>();
        playerInput = GetComponent<PlayerInput>();
        playerInteraction = GetComponent<PlayerInteraction>();
    }

    public void HandleAbilities()
    {
        Dash();
        Target();
    }

    private void Dash()
    {
        if (playerInput.dashFlag && playerInput.isGrounded)
        {
            playerAnimations.SwitchAnimationState(ConstanteVariable.charging);

            maxTimerDash -= Time.deltaTime;

            if (maxTimerDash <= 0)
            {
                Debug.Log("DashTimer Finish !");
                playerAnimations.SwitchAnimationState(ConstanteVariable.dash);
                Vector3 destination = Vector3.forward * maxRangeDash;
                Vector3 dashDistance = destination - transform.position;
                transform.Translate(destination * 100 * Time.deltaTime);
                playerInput.dashFlag = false;
                maxTimerDash = 1;
            }

            playerInput.jumpFlag = false;
        }
        else
        {
            maxTimerDash = 0.1f;
        }

        if (!playerInput.isGrounded)
        {
            playerInput.dashFlag = false;
        }
    }

    private void Target()
    {
        ennemis = playerInteraction.ennemiInside;

        if (playerInput.targetFlag)
        {
            if (playerInput.SwitchLeftFlag)
            {
                index -= 1;
                playerInput.SwitchLeftFlag = false;

                if (index <= 0)
                {
                    index = ennemis.Length - 1;
                }

            }
            if (playerInput.SwitchRightFlag)
            {
                index += 1;
                playerInput.SwitchRightFlag = false;

                if (index >= ennemis.Length)
                {
                    index = 0;
                }
            }

            if(ennemis.Length != 0)
            {
                Transform targetEnnemi = ennemis[index].transform;
                Vcamera.m_LookAt = targetEnnemi;
                Vector3 targetennemiVisual = new Vector3(targetEnnemi.position.x, targetEnnemi.position.y + 2.5f, targetEnnemi.position.z);

                foreach (var targetDisplay in ennemis)
                {
                    if(targetDisplay == ennemis[index] && targetEnnemi.childCount < 2)
                    {
                        GameObject GO = Instantiate(targetVisual, targetennemiVisual, Quaternion.Euler(targetEnnemi.rotation.x, targetEnnemi.rotation.y, 180));
                        GO.transform.parent = targetEnnemi;
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                Transform PlayerFollow = GameObject.FindGameObjectWithTag("Player").transform;
                Vcamera.m_LookAt = PlayerFollow;
                index = 0;
                playerInput.targetFlag = false;
            }
        }
        else
        {
            Transform PlayerFollow = GameObject.FindGameObjectWithTag("Player").transform;
            Vcamera.m_LookAt = PlayerFollow;
            index = 0;
            playerInput.targetFlag = false;
        }
    }
}
