using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaster : MonoBehaviour
{
    private PlayerLocomotion playerLocomotion;
    private PlayerAbilites playerAbilites;

    private void Awake()
    {
        playerLocomotion = GetComponent<PlayerLocomotion>();
        playerAbilites = GetComponent<PlayerAbilites>();
    }

    private void FixedUpdate()
    {
        playerLocomotion.HandleLocomotion();
        playerAbilites.HandleAbilities();
    }
}
