using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocomotion : MonoBehaviour
{
    private PlayerInput playerInput;
    private PlayerAnimations playerAnimations;

    private CharacterController characterController;
    private Transform cam;

    public Transform groundCheck;
    public LayerMask groundMask;

    [HideInInspector] public Vector3 velocity;

    [Header("player settings")]
    public float movementSpeed;
    public float rotationSpeed;
    public float jumpForce;
    public float gravity = -9.84f;
    public float groundDistance = 0.2f;
    public float jumpCount = 2;

    private float turnSmoothTime = 0.1f;
    public float smoothTimeVelocity;


    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        cam = Camera.main.transform;

        playerInput = GetComponent<PlayerInput>();
        playerAnimations = GetComponent<PlayerAnimations>();
    }

    public void HandleLocomotion()
    {
        PlayerMove();
        PlayerJump();
    }

    private void PlayerMove()
    {
        playerInput.isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (playerInput.isGrounded && velocity.y < 0)
        {
            velocity.y = -2;
            jumpCount = 2;
        }

        if (!playerInput.dashFlag)
        {
            Vector3 movementDir = new Vector3(playerInput.horizontalInput, 0, playerInput.verticalInput).normalized;

            if (movementDir.magnitude >= 0.1f)
            {
                float targetAngle = Mathf.Atan2(movementDir.x, movementDir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothTime, smoothTimeVelocity);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 MoveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                characterController.Move(MoveDir.normalized * movementSpeed * Time.deltaTime);
            }

            velocity.y += gravity * Time.deltaTime;
            characterController.Move(velocity * Time.deltaTime);

            if (playerInput.isGrounded)
            {
                if (movementDir.x != 0 || movementDir.z != 0)
                {
                    playerAnimations.SwitchAnimationState(ConstanteVariable.walking);
                }
                else
                {
                    playerAnimations.SwitchAnimationState(ConstanteVariable.idle);
                }
            }
            else
            {
                if (jumpCount == 1 || jumpCount == 2)
                {
                    playerAnimations.SwitchAnimationState(ConstanteVariable.jump);
                }
            }
        }
    }

    private void PlayerJump()
    {
        if (!playerInput.dashFlag)
        {
            if (playerInput.jumpFlag && playerInput.isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpForce * -2.0f * gravity);
                jumpCount -= 1;
                playerInput.jumpFlag = false;
                playerAnimations.SwitchAnimationState(ConstanteVariable.jump);
            }
            else
            {
                if (playerInput.jumpFlag && jumpCount > 0)
                {
                    velocity.y = Mathf.Sqrt(jumpForce * -2.0f * gravity);
                    jumpCount -= 1;
                    playerInput.jumpFlag = false;
                    playerAnimations.SwitchAnimationState(ConstanteVariable.jumpup);
                }
                else
                {
                    playerInput.jumpFlag = false;
                }
            }
        }
    }
}
